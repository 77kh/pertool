from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


def layuitable(request):
    return render(request, 'layuitablelearn.html')


def data_table(request):
    page = int(request.GET.get('page', 0))
    limit = int(request.GET.get('limit', 0))
    sql_data = [
        {"id": '001', "name": "张三", "gender": "男", "age": 18, "city": "北京"},
        {"id": '002', "name": "linda", "gender": "女", "age": 18, "city": "上海"},
        {"id": '003', "name": "lucy", "gender": "女", "age": 18, "city": "昆山"},
        {"id": '004', "name": "bob", "gender": "男", "age": 18, "city": "苏州"},
        {"id": '005', "name": "tom", "gender": "男", "age": 18, "city": "上海"},
        {"id": '006', "name": "kaka", "gender": "男", "age": 18, "city": "上海"},
        {"id": '007', "name": "李四", "gender": "男", "age": 18, "city": "北京"}
    ]
    obj_page_data = sql_data[(page - 1) * limit: page * limit]
    data = {
        "code": 0,
        "msg": "",
        "count": len(sql_data),
        "data": obj_page_data
    }
    return JsonResponse(data=data)


@csrf_exempt
def login(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    authcode = request.POST.get('authcode', '')
    if username == 'lucy' and password == 'abc123':
        data = {
            'code': 1,
            'msg': '登陆成功！'
        }
    else:
        data = {
            'code': 2,
            'msg': '登陆失败！'
        }
    return JsonResponse(data=data)
